<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = ['expense_reason', 'expense_amount'];

    public function give_me_info_of_category()
     {
         return $this->hasOne('App\Category', 'id', 'category_id') -> withTrashed();
     }
}
