<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\poribar;

class AboutController extends Controller
{
    public function shakir()
    {
        $val = User::count();
        $val_poribar = poribar::count();
        return view('about', compact('val','val_poribar'));
    }
}
