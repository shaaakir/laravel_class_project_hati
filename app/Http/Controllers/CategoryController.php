<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Carbon\Carbon;
use Auth;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function categoryinsert(Request $request)
    {
      $request->validate([
          'category_name' => 'required',
      ]);

      Category::insert([
        'added_by' => Auth::id(),
        'category_name' => $request->category_name,
        'created_at' => Carbon::now()
      ]);
      return back();
    }
    public function categoryview()
    {
      $expense_categories = Category::all();
      return view("category/view",compact("expense_categories"));
    }

    public function categorydelete($category_id)
    {
      Category::find($category_id)->delete();
      return back();
    }
}
