<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\User;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    function profileview(){
        return view('profile/view');
    }

    function profileinsert(Request $request){
        if($request -> has('profile_photo')){

            if (User::find(Auth::user()->id)->profile_photo!='photos/default.jpg')
            {
                storage::delete(User::find(Auth::user()->id)->profile_photo);
                $path = $request->file('profile_photo')->store('photos');
                User::find(Auth::user()->id)->update([
                        'profile_photo'=>$path
                ]);
                return back();
            }
            else {
                $path = $request->file('profile_photo')->store('photos');
                User::find(Auth::user()->id)->update([
                        'profile_photo'=>$path
                ]);
                return back();
            }
        }
        else {
            echo "Please Provide Picture !";
        }
    }
}
