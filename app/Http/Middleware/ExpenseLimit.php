<?php

namespace App\Http\Middleware;

use Closure;
use App\Expense;
use Carbon\Carbon;

class ExpenseLimit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $today_expense = Expense::whereDate('created_at', Carbon::now()->format('Y-m-d'))->sum('expense_amount');

        if ($today_expense >= 2000) {
            return redirect('expense/limit');
        }

        return $next($request);
    }
}
