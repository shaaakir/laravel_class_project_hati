-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2018 at 05:14 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hatir_laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `added_by` int(11) NOT NULL,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `added_by`, `category_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'daily', '2018-08-07 03:19:14', '2018-08-08 02:40:39', '2018-08-08 02:40:39'),
(2, 1, 'KDSFLK', '2018-08-07 03:45:47', '2018-08-08 02:40:36', '2018-08-08 02:40:36'),
(3, 1, 'daily', '2018-08-08 10:08:09', NULL, NULL),
(4, 1, 'yearly', '2018-08-08 13:47:33', '2018-08-08 23:21:42', '2018-08-08 23:21:42'),
(5, 1, 'daily', '2018-08-08 23:58:19', '2018-08-11 04:28:27', '2018-08-11 04:28:27'),
(6, 1, 'asf', '2018-08-08 23:58:23', '2018-08-11 04:28:52', '2018-08-11 04:28:52'),
(7, 1, 'yearly', '2018-08-11 04:28:58', '2018-08-11 04:29:56', '2018-08-11 04:29:56'),
(8, 1, 'monthly', '2018-08-11 04:30:02', NULL, NULL),
(9, 1, 'yearly', '2018-08-11 04:30:10', NULL, NULL),
(10, 1, 'hourly', '2018-08-11 04:30:15', NULL, NULL),
(11, 1, 'per minute', '2018-08-11 04:30:24', NULL, NULL),
(12, 1, 'per second', '2018-08-11 04:30:29', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `expense_reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expense_amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expenses`
--

INSERT INTO `expenses` (`id`, `category_id`, `expense_reason`, `expense_amount`, `created_at`, `updated_at`) VALUES
(2, 4, 'ashf;j', 4000000, '2018-08-08 13:47:44', NULL),
(3, 3, 'nasta', 50, '2018-08-08 23:35:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_09_105016_create_poribars_table', 2),
(5, '2018_08_07_090020_create_categories_table', 4),
(6, '2018_07_11_071758_create_expenses_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('shakir@gmail.com', '$2y$10$KHzUl39PIdYzZm5m0eJ72.sPBZCZ3Ilyh6ztmkUBWvcSxPcJH6V8i', '2018-08-15 13:53:18');

-- --------------------------------------------------------

--
-- Table structure for table `poribars`
--

CREATE TABLE `poribars` (
  `id` int(10) UNSIGNED NOT NULL,
  `sodossho_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `poribars`
--

INSERT INTO `poribars` (`id`, `sodossho_name`, `age`, `created_at`, `updated_at`) VALUES
(1, 'shakir', 24, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'photos/default.jpg',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `profile_photo`, `created_at`, `updated_at`) VALUES
(1, 'SHAKIR', 'shakir@gmail.com', '$2y$10$YkS1A.VJTwQdRI235gHgpeLYLqf1uCMvLqJh/PxWDgWR/h1BkVmoa', 'YyRYNuytzAsFSkjwIwnchJcrV3qmW9Mk6sRiOE4YTuHgspKZy5LNGbPQmIi8', 'photos/k57rn320azbhgnYHgIUyWoW1SQ1sfvAsoQ2JmpwL.png', '2018-07-22 05:54:17', '2018-08-06 22:55:14'),
(2, 'shakir', 'sakir3580@gmail.com', '$2y$10$IxV6VbEZI3fU.Fwrg4urQO8zaWGDHGrL2NARxwjnapLV/f50kIzMm', 'PXOTNec02CyOWiaUWYFo9EDC2qDcIT4O6Qkbter6EuQp62KJsksLpKkxAS4Y', 'photos/default.jpg', '2018-08-06 17:59:00', '2018-08-06 17:59:00'),
(3, 'Neil', 'neil@yahoo.com', '$2y$10$dwXEybTMQ1U/9tynq2rGweO0CirV5e8d7vQTOBsw9tNpskE6RO9Pa', NULL, 'photos/default.jpg', '2018-08-06 18:17:32', '2018-08-06 18:17:32'),
(4, 'shakir', 'fahadkhan8751@gmail.com', '$2y$10$x6z16Vnjjh17cTF9EXhkOOvNaVPhZgPd9TIxvJ7L.FEZAql7maStu', NULL, 'photos/default.jpg', '2018-08-06 18:19:19', '2018-08-06 18:19:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `poribars`
--
ALTER TABLE `poribars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `poribars`
--
ALTER TABLE `poribars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
