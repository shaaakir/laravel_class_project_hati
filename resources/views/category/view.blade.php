@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <div class="card-header">
                  List Category
              </div>
              <div class="card-body">
              <table class="table table-striped">
                  <tr>
                      <th>Category Name</th>
                      <th>Added By</th>
                      <th>Action</th>
                  </tr>

                  @forelse($expense_categories as $expense_category)
                  <tr>
                      <td> {{ $expense_category -> category_name }} </td>
                      <td> {{ title_case($expense_category -> give_me_info_of_user -> name) }} </td>

                      </td>
                      <td>
                          <a href="{{ route('categorydelete',$expense_category->id) }}">Delete</a>
                      </td>
                  </tr>
                  @empty
                  <tr>
                    <td>No Data Available .</td>
                  </tr>
                  @endforelse
              </table>
              </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Add Category
                </div>
                <div class="card-body">
                  <form class="form" action="{{route('categoryinsert')}}" method="post">
                    @csrf
                    <label> Category Name </label>
                    <input class="form-control" type="text" name="category_name">
                    <button class="btn btn-success "type="submit">Add Category</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
