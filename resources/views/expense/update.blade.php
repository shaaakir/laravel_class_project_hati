@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Edit Expense
                </div>

                <div class="card-body">
                    <form class="form" action=" {{ url('/expense/update') }}" method="post">
                        @csrf

                        <fieldset class="form-group">
                            <label for="">Expense Reason</label>
                            <input class="form-control" type="hidden" name="expense_id" value="{{ $individual_expense -> id }}">
                            <input class="form-control{{ $errors->has('expense_reason') ? ' is-invalid' : '' }}" type="text" name="expense_reason" value="{{ $individual_expense -> expense_reason }}">
                            @if ($errors->has('expense_reason'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('expense_reason') }}</strong>
                                </span>
                            @endif
                        </fieldset>
                        <fieldset class="form-group">
                            <label for="">Expense Amount</label>
                            <input class="form-control{{ $errors->has('expense_amount') ? ' is-invalid' : '' }}" type="number" name="expense_amount" value="{{ $individual_expense -> expense_amount }}">
                            @if ($errors->has('expense_amount'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('expense_amount') }}</strong>
                                </span>
                            @endif
                        </fieldset>
                        <fieldset class="text-center">
                            <button style="position:center" class="btn btn-warning" type="submit" >UPDATE</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
