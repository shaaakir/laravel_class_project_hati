@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ Auth::user()->name }}'s Profile</div>

                <div class="card-body">
                    <img src="{{ asset('storage') }}/{{Auth::user()->profile_photo}}" alt="NO PHOTO FOUND ." width="100" height="100">
                    <form class="form" action="{{ route('profileinsert') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input class="form-control" type="file" name="profile_photo" value="">
                        <button class="btn btn-success" type="submit">Upload</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
