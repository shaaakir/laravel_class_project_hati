<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::prefix('')->group(function (){
  Route::get('/', 'HomeController@index')->name('home');
});

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/about', 'AboutController@shakir')->name('');

Route::get('/expense/add', 'ExpenseController@expenseadd')->name('')->middleware('expenselimit');

Route::get('/expense/list/view', 'ExpenseController@expenselist')->name('expenselist');

Route::get('/expense/edit/{expense_id}', 'ExpenseController@expenseedit')->name('');

Route::get('/expense/delete/{expense_id}', 'ExpenseController@expensedelete')->name('');

Route::post('/expense/insert', 'ExpenseController@expenseinsert')->name('');

Route::post('/expense/update', 'ExpenseController@expenseupdate')->name('');

Route::get('/expense/limit', 'ExpenseController@expenselimit')->name('');

Route::get('/profile/view', 'ProfileController@profileview')->name('');

Route::post('/profile/insert', 'ProfileController@profileinsert')->name('profileinsert');

Route::get('/category/view', 'CategoryController@categoryview')->name('categoryview');

Route::post('/category/insert', 'CategoryController@categoryinsert')->name('categoryinsert');

Route::get('/category/delete/{category_id}', 'CategoryController@categorydelete')->name('categorydelete');
